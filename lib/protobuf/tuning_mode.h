/*
 *  Copyright (C) 2020-2021 Embedded AMS B.V. - All Rights Reserved
 *
 *  This file is part of Embedded Proto.
 *
 *  Embedded Proto is open source software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 *  Embedded Proto  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Embedded Proto. If not, see <https://www.gnu.org/licenses/>.
 *
 *  For commercial and closed source application please visit:
 *  <https://EmbeddedProto.com/license/>.
 *
 *  Embedded AMS B.V.
 *  Info:
 *    info at EmbeddedProto dot com
 *
 *  Postal address:
 *    Johan Huizingalaan 763a
 *    1066 VH, Amsterdam
 *    the Netherlands
 */

// This file is generated. Please do not edit!
#ifndef TUNING_MODE_H
#define TUNING_MODE_H

#include <cstdint>
#include <MessageInterface.h>
#include <WireFormatter.h>
#include <Fields.h>
#include <MessageSizeCalculator.h>
#include <ReadBufferSection.h>
#include <RepeatedFieldFixedSize.h>
#include <FieldStringBytes.h>
#include <Errors.h>
#include <limits>

// Include external proto definitions


template<uint32_t preamble_LENGTH, 
uint32_t sync_word_LENGTH>
class fsk final: public ::EmbeddedProto::MessageInterface
{
  public:
    fsk() = default;
    fsk(const fsk& rhs )
    {
      set_baudrate(rhs.get_baudrate());
      set_modulation_index(rhs.get_modulation_index());
      set_preamble(rhs.get_preamble());
      set_sync_word(rhs.get_sync_word());
      set_frame_size_fixed(rhs.get_frame_size_fixed());
      set_frame_size(rhs.get_frame_size());
      set_crc(rhs.get_crc());
      set_whitening(rhs.get_whitening());
    }

    fsk(const fsk&& rhs ) noexcept
    {
      set_baudrate(rhs.get_baudrate());
      set_modulation_index(rhs.get_modulation_index());
      set_preamble(rhs.get_preamble());
      set_sync_word(rhs.get_sync_word());
      set_frame_size_fixed(rhs.get_frame_size_fixed());
      set_frame_size(rhs.get_frame_size());
      set_crc(rhs.get_crc());
      set_whitening(rhs.get_whitening());
    }

    ~fsk() override = default;

    enum class crc_scheme : uint32_t
    {
      NO_CRC = 0,
      CRC32_C = 1,
      CRC16_CCIT = 2
    };

    enum class whitening_scheme : uint32_t
    {
      NO_WHITENING = 0,
      G3RUH = 1,
      CCSDS = 2
    };

    enum class id : uint32_t
    {
      NOT_SET = 0,
      BAUDRATE = 1,
      MODULATION_INDEX = 2,
      PREAMBLE = 3,
      SYNC_WORD = 5,
      FRAME_SIZE_FIXED = 6,
      FRAME_SIZE = 7,
      CRC = 8,
      WHITENING = 9
    };

    fsk& operator=(const fsk& rhs)
    {
      set_baudrate(rhs.get_baudrate());
      set_modulation_index(rhs.get_modulation_index());
      set_preamble(rhs.get_preamble());
      set_sync_word(rhs.get_sync_word());
      set_frame_size_fixed(rhs.get_frame_size_fixed());
      set_frame_size(rhs.get_frame_size());
      set_crc(rhs.get_crc());
      set_whitening(rhs.get_whitening());
      return *this;
    }

    fsk& operator=(const fsk&& rhs) noexcept
    {
      set_baudrate(rhs.get_baudrate());
      set_modulation_index(rhs.get_modulation_index());
      set_preamble(rhs.get_preamble());
      set_sync_word(rhs.get_sync_word());
      set_frame_size_fixed(rhs.get_frame_size_fixed());
      set_frame_size(rhs.get_frame_size());
      set_crc(rhs.get_crc());
      set_whitening(rhs.get_whitening());
      return *this;
    }

    inline void clear_baudrate() { baudrate_.clear(); }
    inline void set_baudrate(const EmbeddedProto::doublefixed& value) { baudrate_ = value; }
    inline void set_baudrate(const EmbeddedProto::doublefixed&& value) { baudrate_ = value; }
    inline EmbeddedProto::doublefixed& mutable_baudrate() { return baudrate_; }
    inline const EmbeddedProto::doublefixed& get_baudrate() const { return baudrate_; }
    inline EmbeddedProto::doublefixed::FIELD_TYPE baudrate() const { return baudrate_.get(); }

    inline void clear_modulation_index() { modulation_index_.clear(); }
    inline void set_modulation_index(const EmbeddedProto::doublefixed& value) { modulation_index_ = value; }
    inline void set_modulation_index(const EmbeddedProto::doublefixed&& value) { modulation_index_ = value; }
    inline EmbeddedProto::doublefixed& mutable_modulation_index() { return modulation_index_; }
    inline const EmbeddedProto::doublefixed& get_modulation_index() const { return modulation_index_; }
    inline EmbeddedProto::doublefixed::FIELD_TYPE modulation_index() const { return modulation_index_.get(); }

    inline void clear_preamble() { preamble_.clear(); }
    inline ::EmbeddedProto::FieldBytes<preamble_LENGTH>& mutable_preamble() { return preamble_; }
    inline void set_preamble(const ::EmbeddedProto::FieldBytes<preamble_LENGTH>& rhs) { preamble_.set(rhs); }
    inline const ::EmbeddedProto::FieldBytes<preamble_LENGTH>& get_preamble() const { return preamble_; }
    inline const uint8_t* preamble() const { return preamble_.get_const(); }

    inline void clear_sync_word() { sync_word_.clear(); }
    inline ::EmbeddedProto::FieldBytes<sync_word_LENGTH>& mutable_sync_word() { return sync_word_; }
    inline void set_sync_word(const ::EmbeddedProto::FieldBytes<sync_word_LENGTH>& rhs) { sync_word_.set(rhs); }
    inline const ::EmbeddedProto::FieldBytes<sync_word_LENGTH>& get_sync_word() const { return sync_word_; }
    inline const uint8_t* sync_word() const { return sync_word_.get_const(); }

    inline void clear_frame_size_fixed() { frame_size_fixed_.clear(); }
    inline void set_frame_size_fixed(const EmbeddedProto::boolean& value) { frame_size_fixed_ = value; }
    inline void set_frame_size_fixed(const EmbeddedProto::boolean&& value) { frame_size_fixed_ = value; }
    inline EmbeddedProto::boolean& mutable_frame_size_fixed() { return frame_size_fixed_; }
    inline const EmbeddedProto::boolean& get_frame_size_fixed() const { return frame_size_fixed_; }
    inline EmbeddedProto::boolean::FIELD_TYPE frame_size_fixed() const { return frame_size_fixed_.get(); }

    inline void clear_frame_size() { frame_size_.clear(); }
    inline void set_frame_size(const EmbeddedProto::uint32& value) { frame_size_ = value; }
    inline void set_frame_size(const EmbeddedProto::uint32&& value) { frame_size_ = value; }
    inline EmbeddedProto::uint32& mutable_frame_size() { return frame_size_; }
    inline const EmbeddedProto::uint32& get_frame_size() const { return frame_size_; }
    inline EmbeddedProto::uint32::FIELD_TYPE frame_size() const { return frame_size_.get(); }

    inline void clear_crc() { crc_ = static_cast<crc_scheme>(0); }
    inline void set_crc(const crc_scheme& value) { crc_ = value; }
    inline void set_crc(const crc_scheme&& value) { crc_ = value; }
    inline const crc_scheme& get_crc() const { return crc_; }
    inline crc_scheme crc() const { return crc_; }

    inline void clear_whitening() { whitening_ = static_cast<whitening_scheme>(0); }
    inline void set_whitening(const whitening_scheme& value) { whitening_ = value; }
    inline void set_whitening(const whitening_scheme&& value) { whitening_ = value; }
    inline const whitening_scheme& get_whitening() const { return whitening_; }
    inline whitening_scheme whitening() const { return whitening_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((0.0 != baudrate_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = baudrate_.serialize_with_id(static_cast<uint32_t>(id::BAUDRATE), buffer, false);
      }

      if((0.0 != modulation_index_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = modulation_index_.serialize_with_id(static_cast<uint32_t>(id::MODULATION_INDEX), buffer, false);
      }

      if(::EmbeddedProto::Error::NO_ERRORS == return_value)
      {
        return_value = preamble_.serialize_with_id(static_cast<uint32_t>(id::PREAMBLE), buffer, false);
      }

      if(::EmbeddedProto::Error::NO_ERRORS == return_value)
      {
        return_value = sync_word_.serialize_with_id(static_cast<uint32_t>(id::SYNC_WORD), buffer, false);
      }

      if((false != frame_size_fixed_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = frame_size_fixed_.serialize_with_id(static_cast<uint32_t>(id::FRAME_SIZE_FIXED), buffer, false);
      }

      if((0U != frame_size_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = frame_size_.serialize_with_id(static_cast<uint32_t>(id::FRAME_SIZE), buffer, false);
      }

      if((static_cast<crc_scheme>(0) != crc_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(crc_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::CRC), buffer, false);
      }

      if((static_cast<whitening_scheme>(0) != whitening_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(whitening_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::WHITENING), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::BAUDRATE:
            return_value = baudrate_.deserialize_check_type(buffer, wire_type);
            break;

          case id::MODULATION_INDEX:
            return_value = modulation_index_.deserialize_check_type(buffer, wire_type);
            break;

          case id::PREAMBLE:
            return_value = preamble_.deserialize_check_type(buffer, wire_type);
            break;

          case id::SYNC_WORD:
            return_value = sync_word_.deserialize_check_type(buffer, wire_type);
            break;

          case id::FRAME_SIZE_FIXED:
            return_value = frame_size_fixed_.deserialize_check_type(buffer, wire_type);
            break;

          case id::FRAME_SIZE:
            return_value = frame_size_.deserialize_check_type(buffer, wire_type);
            break;

          case id::CRC:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_crc(static_cast<crc_scheme>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::WHITENING:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_whitening(static_cast<whitening_scheme>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_baudrate();
      clear_modulation_index();
      clear_preamble();
      clear_sync_word();
      clear_frame_size_fixed();
      clear_frame_size();
      clear_crc();
      clear_whitening();

    }

    private:


      EmbeddedProto::doublefixed baudrate_ = 0.0;
      EmbeddedProto::doublefixed modulation_index_ = 0.0;
      ::EmbeddedProto::FieldBytes<preamble_LENGTH> preamble_;
      ::EmbeddedProto::FieldBytes<sync_word_LENGTH> sync_word_;
      EmbeddedProto::boolean frame_size_fixed_ = false;
      EmbeddedProto::uint32 frame_size_ = 0U;
      crc_scheme crc_ = static_cast<crc_scheme>(0);
      whitening_scheme whitening_ = static_cast<whitening_scheme>(0);

};

class lora_implicit_header final: public ::EmbeddedProto::MessageInterface
{
  public:
    lora_implicit_header() = default;
    lora_implicit_header(const lora_implicit_header& rhs )
    {
      set_len(rhs.get_len());
      set_crc(rhs.get_crc());
      set_cr(rhs.get_cr());
    }

    lora_implicit_header(const lora_implicit_header&& rhs ) noexcept
    {
      set_len(rhs.get_len());
      set_crc(rhs.get_crc());
      set_cr(rhs.get_cr());
    }

    ~lora_implicit_header() override = default;

    enum class coding_rate : uint32_t
    {
      CR_4_5 = 0,
      CR_4_6 = 1,
      CR_4_7 = 2,
      CR_4_8 = 3
    };

    enum class id : uint32_t
    {
      NOT_SET = 0,
      LEN = 1,
      CRC = 2,
      CR = 3
    };

    lora_implicit_header& operator=(const lora_implicit_header& rhs)
    {
      set_len(rhs.get_len());
      set_crc(rhs.get_crc());
      set_cr(rhs.get_cr());
      return *this;
    }

    lora_implicit_header& operator=(const lora_implicit_header&& rhs) noexcept
    {
      set_len(rhs.get_len());
      set_crc(rhs.get_crc());
      set_cr(rhs.get_cr());
      return *this;
    }

    inline void clear_len() { len_.clear(); }
    inline void set_len(const EmbeddedProto::uint32& value) { len_ = value; }
    inline void set_len(const EmbeddedProto::uint32&& value) { len_ = value; }
    inline EmbeddedProto::uint32& mutable_len() { return len_; }
    inline const EmbeddedProto::uint32& get_len() const { return len_; }
    inline EmbeddedProto::uint32::FIELD_TYPE len() const { return len_.get(); }

    inline void clear_crc() { crc_.clear(); }
    inline void set_crc(const EmbeddedProto::boolean& value) { crc_ = value; }
    inline void set_crc(const EmbeddedProto::boolean&& value) { crc_ = value; }
    inline EmbeddedProto::boolean& mutable_crc() { return crc_; }
    inline const EmbeddedProto::boolean& get_crc() const { return crc_; }
    inline EmbeddedProto::boolean::FIELD_TYPE crc() const { return crc_.get(); }

    inline void clear_cr() { cr_ = static_cast<coding_rate>(0); }
    inline void set_cr(const coding_rate& value) { cr_ = value; }
    inline void set_cr(const coding_rate&& value) { cr_ = value; }
    inline const coding_rate& get_cr() const { return cr_; }
    inline coding_rate cr() const { return cr_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((0U != len_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = len_.serialize_with_id(static_cast<uint32_t>(id::LEN), buffer, false);
      }

      if((false != crc_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = crc_.serialize_with_id(static_cast<uint32_t>(id::CRC), buffer, false);
      }

      if((static_cast<coding_rate>(0) != cr_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(cr_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::CR), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::LEN:
            return_value = len_.deserialize_check_type(buffer, wire_type);
            break;

          case id::CRC:
            return_value = crc_.deserialize_check_type(buffer, wire_type);
            break;

          case id::CR:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_cr(static_cast<coding_rate>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_len();
      clear_crc();
      clear_cr();

    }

    private:


      EmbeddedProto::uint32 len_ = 0U;
      EmbeddedProto::boolean crc_ = false;
      coding_rate cr_ = static_cast<coding_rate>(0);

};

class lora final: public ::EmbeddedProto::MessageInterface
{
  public:
    lora() = default;
    lora(const lora& rhs )
    {
      set_sync(rhs.get_sync());
      set_sf(rhs.get_sf());
      set_bw(rhs.get_bw());
      set_header(rhs.get_header());
    }

    lora(const lora&& rhs ) noexcept
    {
      set_sync(rhs.get_sync());
      set_sf(rhs.get_sf());
      set_bw(rhs.get_bw());
      set_header(rhs.get_header());
    }

    ~lora() override = default;

    enum class spreading_factor : uint32_t
    {
      SF_6 = 0,
      SF_7 = 1,
      SF_8 = 2,
      SF_9 = 3,
      SF_10 = 4,
      SF_11 = 5,
      SF_12 = 6
    };

    enum class bandwidth : uint32_t
    {
      BW_7 = 0,
      BW_10 = 1,
      BW_15 = 2,
      BW_20 = 3,
      BW_31 = 4,
      BW_41 = 5,
      BW_62 = 6,
      BW_125 = 7,
      BW_250 = 8,
      BW_500 = 9
    };

    enum class id : uint32_t
    {
      NOT_SET = 0,
      SYNC = 1,
      SF = 2,
      BW = 3,
      HEADER = 4
    };

    lora& operator=(const lora& rhs)
    {
      set_sync(rhs.get_sync());
      set_sf(rhs.get_sf());
      set_bw(rhs.get_bw());
      set_header(rhs.get_header());
      return *this;
    }

    lora& operator=(const lora&& rhs) noexcept
    {
      set_sync(rhs.get_sync());
      set_sf(rhs.get_sf());
      set_bw(rhs.get_bw());
      set_header(rhs.get_header());
      return *this;
    }

    inline void clear_sync() { sync_.clear(); }
    inline void set_sync(const EmbeddedProto::uint32& value) { sync_ = value; }
    inline void set_sync(const EmbeddedProto::uint32&& value) { sync_ = value; }
    inline EmbeddedProto::uint32& mutable_sync() { return sync_; }
    inline const EmbeddedProto::uint32& get_sync() const { return sync_; }
    inline EmbeddedProto::uint32::FIELD_TYPE sync() const { return sync_.get(); }

    inline void clear_sf() { sf_ = static_cast<spreading_factor>(0); }
    inline void set_sf(const spreading_factor& value) { sf_ = value; }
    inline void set_sf(const spreading_factor&& value) { sf_ = value; }
    inline const spreading_factor& get_sf() const { return sf_; }
    inline spreading_factor sf() const { return sf_; }

    inline void clear_bw() { bw_ = static_cast<bandwidth>(0); }
    inline void set_bw(const bandwidth& value) { bw_ = value; }
    inline void set_bw(const bandwidth&& value) { bw_ = value; }
    inline const bandwidth& get_bw() const { return bw_; }
    inline bandwidth bw() const { return bw_; }

    inline bool has_header() const
    {
      return 0 != (presence::mask(presence::fields::HEADER) & presence_[presence::index(presence::fields::HEADER)]);
    }
    inline void clear_header()
    {
      presence_[presence::index(presence::fields::HEADER)] &= ~(presence::mask(presence::fields::HEADER));
      header_.clear();
    }
    inline void set_header(const lora_implicit_header& value)
    {
      presence_[presence::index(presence::fields::HEADER)] |= presence::mask(presence::fields::HEADER);
      header_ = value;
    }
    inline void set_header(const lora_implicit_header&& value)
    {
      presence_[presence::index(presence::fields::HEADER)] |= presence::mask(presence::fields::HEADER);
      header_ = value;
    }
    inline lora_implicit_header& mutable_header()
    {
      presence_[presence::index(presence::fields::HEADER)] |= presence::mask(presence::fields::HEADER);
      return header_;
    }
    inline const lora_implicit_header& get_header() const { return header_; }
    inline const lora_implicit_header& header() const { return header_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((0U != sync_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = sync_.serialize_with_id(static_cast<uint32_t>(id::SYNC), buffer, false);
      }

      if((static_cast<spreading_factor>(0) != sf_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(sf_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::SF), buffer, false);
      }

      if((static_cast<bandwidth>(0) != bw_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(bw_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::BW), buffer, false);
      }

      if(has_header() && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = header_.serialize_with_id(static_cast<uint32_t>(id::HEADER), buffer, true);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::SYNC:
            return_value = sync_.deserialize_check_type(buffer, wire_type);
            break;

          case id::SF:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_sf(static_cast<spreading_factor>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::BW:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_bw(static_cast<bandwidth>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::HEADER:
            presence_[presence::index(presence::fields::HEADER)] |= presence::mask(presence::fields::HEADER);
            return_value = header_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_sync();
      clear_sf();
      clear_bw();
      clear_header();

    }

    private:

      // Define constants for tracking the presence of fields.
      // Use a struct to scope the variables from user fields as namespaces are not allowed within classes.
      struct presence
      {
        // An enumeration with all the fields for which presence has to be tracked.
        enum class fields : uint32_t
        {
          HEADER
        };

        // The number of fields for which presence has to be tracked.
        static constexpr uint32_t N_FIELDS = 1;

        // Which type are we using to track presence.
        using TYPE = uint32_t;

        // How many bits are there in the presence type.
        static constexpr uint32_t N_BITS = std::numeric_limits<TYPE>::digits;

        // How many variables of TYPE do we need to bit mask all presence fields.
        static constexpr uint32_t SIZE = (N_FIELDS / N_BITS) + ((N_FIELDS % N_BITS) > 0 ? 1 : 0);

        // Obtain the index of a given field in the presence array.
        static constexpr uint32_t index(const fields& field) { return static_cast<uint32_t>(field) / N_BITS; }

        // Obtain the bit mask for the given field assuming we are at the correct index in the presence array.
        static constexpr TYPE mask(const fields& field)
        {
          return static_cast<uint32_t>(0x01) << (static_cast<uint32_t>(field) % N_BITS);
        }
      };

      // Create an array in which the presence flags are stored.
      typename presence::TYPE presence_[presence::SIZE] = {0};

      EmbeddedProto::uint32 sync_ = 0U;
      spreading_factor sf_ = static_cast<spreading_factor>(0);
      bandwidth bw_ = static_cast<bandwidth>(0);
      lora_implicit_header header_;

};

template<uint32_t fsk_params_preamble_LENGTH, 
uint32_t fsk_params_sync_word_LENGTH>
class tuning_mode final: public ::EmbeddedProto::MessageInterface
{
  public:
    tuning_mode() = default;
    tuning_mode(const tuning_mode& rhs )
    {
      set_mode(rhs.get_mode());
      set_fsk_params(rhs.get_fsk_params());
      set_lora_params(rhs.get_lora_params());
    }

    tuning_mode(const tuning_mode&& rhs ) noexcept
    {
      set_mode(rhs.get_mode());
      set_fsk_params(rhs.get_fsk_params());
      set_lora_params(rhs.get_lora_params());
    }

    ~tuning_mode() override = default;

    enum class modulation_mode : uint32_t
    {
      FSK = 0,
      LORA = 1
    };

    enum class id : uint32_t
    {
      NOT_SET = 0,
      MODE = 1,
      FSK_PARAMS = 2,
      LORA_PARAMS = 3
    };

    tuning_mode& operator=(const tuning_mode& rhs)
    {
      set_mode(rhs.get_mode());
      set_fsk_params(rhs.get_fsk_params());
      set_lora_params(rhs.get_lora_params());
      return *this;
    }

    tuning_mode& operator=(const tuning_mode&& rhs) noexcept
    {
      set_mode(rhs.get_mode());
      set_fsk_params(rhs.get_fsk_params());
      set_lora_params(rhs.get_lora_params());
      return *this;
    }

    inline void clear_mode() { mode_ = static_cast<modulation_mode>(0); }
    inline void set_mode(const modulation_mode& value) { mode_ = value; }
    inline void set_mode(const modulation_mode&& value) { mode_ = value; }
    inline const modulation_mode& get_mode() const { return mode_; }
    inline modulation_mode mode() const { return mode_; }

    inline bool has_fsk_params() const
    {
      return 0 != (presence::mask(presence::fields::FSK_PARAMS) & presence_[presence::index(presence::fields::FSK_PARAMS)]);
    }
    inline void clear_fsk_params()
    {
      presence_[presence::index(presence::fields::FSK_PARAMS)] &= ~(presence::mask(presence::fields::FSK_PARAMS));
      fsk_params_.clear();
    }
    inline void set_fsk_params(const fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH>& value)
    {
      presence_[presence::index(presence::fields::FSK_PARAMS)] |= presence::mask(presence::fields::FSK_PARAMS);
      fsk_params_ = value;
    }
    inline void set_fsk_params(const fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH>&& value)
    {
      presence_[presence::index(presence::fields::FSK_PARAMS)] |= presence::mask(presence::fields::FSK_PARAMS);
      fsk_params_ = value;
    }
    inline fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH>& mutable_fsk_params()
    {
      presence_[presence::index(presence::fields::FSK_PARAMS)] |= presence::mask(presence::fields::FSK_PARAMS);
      return fsk_params_;
    }
    inline const fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH>& get_fsk_params() const { return fsk_params_; }
    inline const fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH>& fsk_params() const { return fsk_params_; }

    inline bool has_lora_params() const
    {
      return 0 != (presence::mask(presence::fields::LORA_PARAMS) & presence_[presence::index(presence::fields::LORA_PARAMS)]);
    }
    inline void clear_lora_params()
    {
      presence_[presence::index(presence::fields::LORA_PARAMS)] &= ~(presence::mask(presence::fields::LORA_PARAMS));
      lora_params_.clear();
    }
    inline void set_lora_params(const lora& value)
    {
      presence_[presence::index(presence::fields::LORA_PARAMS)] |= presence::mask(presence::fields::LORA_PARAMS);
      lora_params_ = value;
    }
    inline void set_lora_params(const lora&& value)
    {
      presence_[presence::index(presence::fields::LORA_PARAMS)] |= presence::mask(presence::fields::LORA_PARAMS);
      lora_params_ = value;
    }
    inline lora& mutable_lora_params()
    {
      presence_[presence::index(presence::fields::LORA_PARAMS)] |= presence::mask(presence::fields::LORA_PARAMS);
      return lora_params_;
    }
    inline const lora& get_lora_params() const { return lora_params_; }
    inline const lora& lora_params() const { return lora_params_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((static_cast<modulation_mode>(0) != mode_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(mode_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::MODE), buffer, false);
      }

      if(has_fsk_params() && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = fsk_params_.serialize_with_id(static_cast<uint32_t>(id::FSK_PARAMS), buffer, true);
      }

      if(has_lora_params() && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = lora_params_.serialize_with_id(static_cast<uint32_t>(id::LORA_PARAMS), buffer, true);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::MODE:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_mode(static_cast<modulation_mode>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::FSK_PARAMS:
            presence_[presence::index(presence::fields::FSK_PARAMS)] |= presence::mask(presence::fields::FSK_PARAMS);
            return_value = fsk_params_.deserialize_check_type(buffer, wire_type);
            break;

          case id::LORA_PARAMS:
            presence_[presence::index(presence::fields::LORA_PARAMS)] |= presence::mask(presence::fields::LORA_PARAMS);
            return_value = lora_params_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_mode();
      clear_fsk_params();
      clear_lora_params();

    }

    private:

      // Define constants for tracking the presence of fields.
      // Use a struct to scope the variables from user fields as namespaces are not allowed within classes.
      struct presence
      {
        // An enumeration with all the fields for which presence has to be tracked.
        enum class fields : uint32_t
        {
          FSK_PARAMS,
          LORA_PARAMS
        };

        // The number of fields for which presence has to be tracked.
        static constexpr uint32_t N_FIELDS = 2;

        // Which type are we using to track presence.
        using TYPE = uint32_t;

        // How many bits are there in the presence type.
        static constexpr uint32_t N_BITS = std::numeric_limits<TYPE>::digits;

        // How many variables of TYPE do we need to bit mask all presence fields.
        static constexpr uint32_t SIZE = (N_FIELDS / N_BITS) + ((N_FIELDS % N_BITS) > 0 ? 1 : 0);

        // Obtain the index of a given field in the presence array.
        static constexpr uint32_t index(const fields& field) { return static_cast<uint32_t>(field) / N_BITS; }

        // Obtain the bit mask for the given field assuming we are at the correct index in the presence array.
        static constexpr TYPE mask(const fields& field)
        {
          return static_cast<uint32_t>(0x01) << (static_cast<uint32_t>(field) % N_BITS);
        }
      };

      // Create an array in which the presence flags are stored.
      typename presence::TYPE presence_[presence::SIZE] = {0};

      modulation_mode mode_ = static_cast<modulation_mode>(0);
      fsk<fsk_params_preamble_LENGTH, fsk_params_sync_word_LENGTH> fsk_params_;
      lora lora_params_;

};

#endif // TUNING_MODE_H