#ifndef STATION_HPP
#define STATION_HPP

#include <stdint.h>
#include <stddef.h>
#include <config.hpp>
#include <protobuf.hpp>
#include "status.hpp"
#include "tuning.hpp"
#include "mqtt_client.hpp"

class station {
public:
  static station&
  instance()
  {
    static station inst;
    return inst;
  }

  void
  setup();

  void
  loop();

  const mqtt_client&
  get_mqtt_client() const;

  const config&
  get_config() const;

  const status&
  get_status() const;

  const tuning&
  get_tuning() const;

  template<uint32_t preamble_LEN, uint32_t sync_word_LEN>
  void
  set_tuning_from_proto(const tuning_mode<preamble_LEN, sync_word_LEN>& tm)
  {
    d_tuning.set_from_proto(tm);
  }

private:
  station();

  void
  wifi_connected();

  bool d_need_mqtt_connect;
  mqtt_client &d_mqtt_client = mqtt_client::instance();
  config &d_cnf = config::instance();
  status &d_status = status::instance();
  tuning d_tuning;
};

#endif // STATION_HPP
