#ifndef TUNING_HPP
#define TUNING_HPP

#include <status.hpp>
#include <protobuf.hpp>
#include "fsk_modulation.hpp"
#include "lora_modulation.hpp"

class tuning {
public:
  union params {
    fsk_modulation fsk_ = fsk_modulation();
    lora_modulation lora_;

    params() {}
  };

  tuning();

  tuning(const tuning &other);

  template<uint32_t preamble_LEN, uint32_t sync_word_LEN>
  tuning(const tuning_mode<preamble_LEN, sync_word_LEN> &tm)
  {
    d_mode = static_cast<status::modulation_mode>(tm.get_mode());
    switch (d_mode) {
    case status::modulation_mode::FSK:
      d_params.fsk_.set_from_proto(tm.get_fsk_params());
      break;
    case status::modulation_mode::LORA:
      d_params.lora_.set_from_proto(tm.get_lora_params());
      break;
    default:
      break;
    }
  }

  template<uint32_t preamble_LEN, uint32_t sync_word_LEN>
  void
  set_from_proto(const tuning_mode<preamble_LEN, sync_word_LEN> &tm)
  {
    d_mode = static_cast<status::modulation_mode>(tm.get_mode());
    switch (d_mode) {
    case status::modulation_mode::FSK:
      d_params.fsk_.set_from_proto(tm.get_fsk_params());
      break;
    case status::modulation_mode::LORA:
      d_params.lora_.set_from_proto(tm.get_lora_params());
      break;
    default:
      break;
    }
  }

  status::modulation_mode
  get_mode() const;

  const fsk_modulation&
  get_fsk_params() const;

  const lora_modulation&
  get_lora_params() const;

private:
  status::modulation_mode d_mode;
  params d_params;
};

#endif // TUNING_HPP
