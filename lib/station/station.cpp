#include <station.hpp>
#include <ESPNtpClient.h>

void
station::setup()
{
  d_need_mqtt_connect = false;
  d_cnf.setWifiConnectionCallback([this] () {
    wifi_connected();
  });
  bool valid_config = d_cnf.init();
  d_mqtt_client.init(valid_config);
}

void
station::loop()
{
  d_cnf.doLoop();
  d_mqtt_client.loop();
  
  if (d_need_mqtt_connect) {
    if (d_mqtt_client.connect()) {
      d_need_mqtt_connect = false;
    }
  }
  else if ((d_cnf.getState() == iotwebconf::OnLine) && (!d_mqtt_client.connected())) {
    Serial.println("MQTT reconnect");
    d_mqtt_client.connect();
  }

  d_cnf.restart_if_needed();
}

const mqtt_client&
station::get_mqtt_client() const
{
  return d_mqtt_client;
}

const config&
station::get_config() const
{
  return d_cnf;
}

const status&
station::get_status() const
{
  return d_status;
}

const tuning&
station::get_tuning() const
{
  return d_tuning;
}

station::station()
{
}

void
station::wifi_connected()
{
  d_need_mqtt_connect = true;
  NTP.setTimeZone(TZ_Etc_UTC);
  NTP.begin();
}
