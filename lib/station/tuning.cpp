#include "tuning.hpp"

tuning::tuning() :
  d_mode(status::modulation_mode::FSK)
{
}

tuning::tuning(const tuning &other) :
  d_mode(status::modulation_mode::FSK)
{
  d_mode = other.get_mode();
  switch (d_mode) {
  case status::modulation_mode::FSK:
    d_params.fsk_ = other.get_fsk_params();
    break;
  case status::modulation_mode::LORA:
    d_params.lora_ = other.get_lora_params();
    break;
  default:
    break;
  }
}

status::modulation_mode
tuning::get_mode() const
{
  return d_mode;
}

const fsk_modulation&
tuning::get_fsk_params() const
{
  if (d_mode == status::modulation_mode::FSK) {
    return d_params.fsk_;
  }
}

const lora_modulation&
tuning::get_lora_params() const
{
  if (d_mode == status::modulation_mode::LORA) {
    return d_params.lora_;
  }
}
