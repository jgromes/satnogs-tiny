#include "fsk_modulation.hpp"
#include <Arduino.h>

fsk_modulation::fsk_modulation()
{
}

fsk_modulation::fsk_modulation(const fsk_modulation &other)
{
  d_baudrate = other.get_baudrate();
  d_modulation_index = other.get_modulation_index();
  d_preamble_len = other.get_preamble_length();
  d_preamble = new uint8_t[d_preamble_len];
  memcpy(d_preamble, other.get_preamble(), d_preamble_len);
  d_sync_word_len = other.get_sync_word_length();
  d_sync_word = new uint8_t[d_sync_word_len];
  memcpy(d_sync_word, other.get_sync_word(), d_sync_word_len);
  d_frame_size_fixed = other.has_fixed_frame_size();
  d_frame_size = other.get_frame_size();
  d_crc = other.get_crc();
  d_whitening = other.get_whitening();
}

double
fsk_modulation::get_baudrate() const
{
  return d_baudrate;
}

double
fsk_modulation::get_modulation_index() const
{
  return d_modulation_index;
}

const uint8_t*
fsk_modulation::get_preamble() const
{
  return d_preamble;
}

size_t
fsk_modulation::get_preamble_length() const
{
  return d_preamble_len;
}

const uint8_t*
fsk_modulation::get_sync_word() const
{
  return d_sync_word;
}

size_t
fsk_modulation::get_sync_word_length() const
{
  return d_sync_word_len;
}

bool
fsk_modulation::has_fixed_frame_size() const
{
  return d_frame_size_fixed;
}

size_t
fsk_modulation::get_frame_size() const
{
  return d_frame_size;
}

fsk_modulation::crc_scheme
fsk_modulation::get_crc() const
{
  return d_crc;
}

fsk_modulation::whitening_scheme
fsk_modulation::get_whitening() const
{
  return d_whitening;
}
