#include "lora_modulation.hpp"

lora_modulation::lora_modulation()
{
}

lora_modulation::lora_modulation(const lora_modulation &other)
{
  d_sync = other.get_sync();
  d_sf = other.get_spreading_factor();
  d_bw = other.get_bandwidth();
  if (other.has_implicit_header()) {
    d_implicit_header = true;
    d_hdr_len = other.get_header_length();
    d_hdr_crc = other.has_header_crc();
    d_hdr_cr = other.get_header_coding_rate();
  }
  else {
    d_implicit_header = false;
  }
}

lora_modulation::lora_modulation(const lora &params)
{
  d_sync = params.get_sync().get();
  d_sf = static_cast<spreading_factor>(params.get_sf());
  d_bw = static_cast<bandwidth>(params.get_bw());
  if (params.has_header()) {
    d_implicit_header = true;
    d_hdr_len = params.get_header().get_len().get();
    d_hdr_crc = params.get_header().get_crc().get();
    d_hdr_cr = static_cast<coding_rate>(params.get_header().get_cr());
  }
  else {
    d_implicit_header = false;
  }
}

void
lora_modulation::set_from_proto(const lora &params)
{
  d_sync = params.get_sync().get();
  d_sf = static_cast<spreading_factor>(params.get_sf());
  d_bw = static_cast<bandwidth>(params.get_bw());
  if (params.has_header()) {
    d_implicit_header = true;
    d_hdr_len = params.get_header().get_len().get();
    d_hdr_crc = params.get_header().get_crc().get();
    d_hdr_cr = static_cast<coding_rate>(params.get_header().get_cr());
  }
  else {
    d_implicit_header = false;
  }
}

size_t
lora_modulation::get_sync() const
{
  return d_sync;
}

lora_modulation::spreading_factor
lora_modulation::get_spreading_factor() const
{
  return d_sf;
}

lora_modulation::bandwidth
lora_modulation::get_bandwidth() const
{
  return d_bw;
}

bool
lora_modulation::has_implicit_header() const
{
  return d_implicit_header;
}

size_t
lora_modulation::get_header_length() const
{
  return d_hdr_len;
}

bool
lora_modulation::has_header_crc() const
{
  return d_hdr_crc;
}

lora_modulation::coding_rate
lora_modulation::get_header_coding_rate() const
{
  return d_hdr_cr;
}
