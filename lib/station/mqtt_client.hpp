#ifndef MQTT_CLIENT_HPP
#define MQTT_CLIENT_HPP

#include <MQTT.h>
#include <WiFi.h>
#include <config.hpp>
#include <protobuf.hpp>

#define BUFFER_SIZE 1024U

/*
 * A singleton class that holds the MQTT Client status
 */
class mqtt_client {
public:
  enum class topic {
    STATUS = 0,
    MODE,
    FREQUENCY,
    FRAME,
    TX_FRAME
  };

public:
  static mqtt_client& instance() {
    static mqtt_client holder;
    return holder;
  }

  void
  init(bool valid_config);

  bool
  connect();

  bool
  connected();

  void
  loop();

  void
  publish(topic t, wbuffer<BUFFER_SIZE> &wb);

  bool
  has_receive_message() const;

  void
  message_was_read();

  const rbuffer<BUFFER_SIZE>&
  get_readbuffer() const;

  topic
  get_topic() const;

private:
  mqtt_client();

  void
  msg_received(MQTTClient *client, uint8_t *topic, uint8_t *bytes, size_t length);

  bool
  connect_credentials();

  void
  subscribe();

  config &d_cnf = config::instance();
  MQTTClient d_client;
  WiFiClient d_wifi_client;
  bool d_msg_received = false;
  rbuffer<BUFFER_SIZE> d_rb;
  topic d_topic;
};

#endif
