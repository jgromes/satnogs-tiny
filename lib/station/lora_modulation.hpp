#ifndef LORA_MODULATION_HPP
#define LORA_MODULATION_HPP

#include <stddef.h>
#include <protobuf.hpp>

class lora_modulation {
public:
  enum class spreading_factor {
    SF_6 = 0,
    SF_7,
    SF_8,
    SF_9,
    SF_10,
    SF_11,
    SF_12
  };

  enum class bandwidth {
    BW_7 = 0,
    BW_10,
    BW_15,
    BW_20,
    BW_31,
    BW_41,
    BW_62,
    BW_125,
    BW_250,
    BW_500
  };

  enum class coding_rate {
    CR_4_5 = 0,
    CR_4_6,
    CR_4_7,
    CR_4_8
  };

  lora_modulation();

  lora_modulation(const lora_modulation &other);

  lora_modulation(const lora &params);

  void
  set_from_proto(const lora &params);

  size_t
  get_sync() const;

  spreading_factor
  get_spreading_factor() const;

  bandwidth
  get_bandwidth() const;

  bool
  has_implicit_header() const;

  size_t
  get_header_length() const;

  bool
  has_header_crc() const;

  coding_rate
  get_header_coding_rate() const;

private:
  size_t d_sync;
  spreading_factor d_sf;
  bandwidth d_bw;
  bool d_implicit_header = false;
  // header parameters
  size_t d_hdr_len;
  bool d_hdr_crc;
  coding_rate d_hdr_cr;
};

#endif // LORA_MODULATION_HPP
