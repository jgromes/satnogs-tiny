#!/usr/bin/env python3

import argparse
from satnogs_tiny_pb2 import fsk_parameters
import paho.mqtt.client as mqtt

BROKER_HOSTNAME = "localhost"
BROKER_PORT = 1883
BROKER_TIMEOUT = 60


def set_modulation(station):
    """
    Set the modulation and framing paramenters
    in the selected station to example values.
    """
    client = mqtt.Client()
    # client.on_connect = on_connect
    try:
        client.connect(BROKER_HOSTNAME,
                       BROKER_PORT,
                       BROKER_TIMEOUT)
    except ConnectionRefusedError:
        print("ERROR: Connection refused, maybe no MQTT broker "
              "running at {}:{} ?".format(BROKER_HOSTNAME, BROKER_PORT))

    params = fsk_parameters()
    params.baudrate = 9600.0
    params.modulation_index = 1.0
    params.preamble = b"\xF0\xF0\xF0\xF0"
    params.sync_word = b"\x91\x12\x99"
    params.frame_size_fixed = True
    params.frame_size = 41
    params.crc = fsk_parameters.NO_CRC
    params.whitening = fsk_parameters.NO_WHITENING

    payload = params.SerializeToString()

    client.publish('/station/{}/mode'.format(station),
                   payload=payload,
                   qos=0,
                   retain=False)
    print("Sent to {}: \n{}".format(station, params))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--station",
                        help="The station ID",
                        type=int,
                        required=True)
    args = parser.parse_args()

    set_modulation(station=args.station)
